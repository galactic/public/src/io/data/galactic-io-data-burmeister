"""Population test module."""

from unittest import TestCase

from galactic.io.data.burmeister import BurmeisterDataReader
from galactic.io.data.core import PopulationFactory


class PopulationTest(TestCase):
    def test_population(self):
        self.assertTrue(
            any(
                isinstance(reader, BurmeisterDataReader)
                for reader in PopulationFactory.readers()
            ),
        )
