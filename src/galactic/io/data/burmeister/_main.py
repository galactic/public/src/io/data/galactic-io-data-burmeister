from collections.abc import Iterator, Mapping
from typing import TextIO, cast

from galactic.helpers.core import default_repr
from galactic.io.data.core import PopulationFactory


# pylint: disable=too-few-public-methods
class BurmeisterDataReader:
    """
    `̀Burmeister`` Data reader.

    Example
    -------
    >>> from galactic.io.data.burmeister import BurmeisterDataReader
    >>> reader = BurmeisterDataReader()
    >>> import io
    >>> data = '''B
    ...
    ... 2
    ... 2
    ...
    ... 1
    ... 2
    ... a
    ... b
    ... .X
    ... XX
    ... '''
    >>> individuals = reader.read(io.StringIO(data))
    >>> [
    ...     (key, sorted(list(values)))
    ...     for key, values in individuals.items()
    ... ]
    [('1', ['b']), ('2', ['a', 'b'])]

    """

    __slots__ = ()

    def __repr__(self) -> str:
        return cast(str, default_repr(self))

    @classmethod
    def read(cls, data_file: TextIO) -> Mapping[str, object]:
        """
        Read a ``Burmeister`` data file.

        Parameters
        ----------
        data_file
            A readable text file.

        Returns
        -------
        Mapping[str, object]
            The data.

        Raises
        ------
        RuntimeError
            If the Burmeister HEADER is not detected

        """
        individuals = {}

        line = data_file.readline()[:-1]
        if line != "B":
            raise RuntimeError("Burmeister HEADER not detected")

        data_file.readline()

        nb_observations = int(data_file.readline())
        nb_attributes = int(data_file.readline())

        data_file.readline()

        # reading observation names
        observation_names = [data_file.readline()[:-1] for _ in range(nb_observations)]

        # reading attributes names
        attributes_names = [data_file.readline()[:-1] for _ in range(nb_attributes)]

        # reading context
        for index_observations in range(nb_observations):
            attributes = set()
            definition = data_file.readline()
            for index_attributes in range(nb_attributes):
                if definition[index_attributes] == "X":
                    attributes.add(attributes_names[index_attributes])
            individuals[observation_names[index_observations]] = frozenset(attributes)
        return individuals

    @classmethod
    def extensions(cls) -> Iterator[str]:
        """
        Get an iterator over the supported extensions.

        Returns
        -------
        Iterator[str]
            An iterator over the supported extensions

        """
        return iter([".cxt"])


def register() -> None:
    """
    Register an instance of a ``Burmeister`` data reader.
    """
    PopulationFactory.register_reader(BurmeisterDataReader())
