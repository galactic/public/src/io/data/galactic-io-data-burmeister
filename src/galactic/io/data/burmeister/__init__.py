"""
Burmeister Data reader.
"""

from ._main import BurmeisterDataReader, register

__all__ = ("BurmeisterDataReader", "register")
