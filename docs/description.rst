========================
*Burmeister* data reader
========================

*galactic-io-data-burmeister* is a data reader
plugin for **GALACTIC**.


The *Burmeister* context format uses the extension ``.cxt``.

Each file is structured as follows:

* the first line consists of a single ``B``;
* the second line is an empty line (its content is ignored);
* the third and fourth lines contain the object and attribute count;
* the fifth line is an empty line (its content is ignored);
* after that, all objects and all attributes are listed, each on a line
* finally, the context is given as a combination of ``.`` and ``X``, each row
  on its own line.

.. code-block:: text
    :class: admonition

    B

    10
    5

    0
    1
    2
    3
    4
    5
    6
    7
    8
    9
    c
    e
    o
    p
    s
    XX..X
    ..X.X
    .X.X.
    ..XX.
    XX..X
    ..XX.
    XX...
    ..XX.
    XX...
    X.X.X


